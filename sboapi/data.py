#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from models.models import SlackBuilds, session


class Data:
    """Creates data from the text file and insert
    them into the database
    """
    def __init__(self, file, version):
        self.file = file
        self.version = version
        self.session = session
        self.sbo = [
            'SLACKBUILD NAME:',
            'SLACKBUILD LOCATION:',
            'SLACKBUILD FILES:',
            'SLACKBUILD VERSION:',
            'SLACKBUILD DOWNLOAD:',
            'SLACKBUILD DOWNLOAD_x86_64:',
            'SLACKBUILD MD5SUM:',
            'SLACKBUILD MD5SUM_x86_64:',
            'SLACKBUILD REQUIRES:',
            'SLACKBUILD SHORT DESCRIPTION:'
        ]

    @staticmethod
    def open_file(file):
        try:
            with open(file, 'r', encoding='utf-8') as f:
                return f.readlines()
        except FileNotFoundError as error:
            raise SystemExit(error)

    def push(self):
        """Reads the file SLACKBUILDS.TXT line per line, splits
        the data and pushing them into the database.
        url: https://slackbuilds.org/slackbuilds/15.0/SLACKBUILDS.TXT
        """
        sbo_file = self.open_file(self.file)

        cache = []  # init cache

        for i, line in enumerate(sbo_file, 1):

            for s in self.sbo:
                if line.startswith(s):
                    line = line.replace(s, '').strip()
                    cache.append(line)

            if (i % 11) == 0:

                sbo_data = SlackBuilds(name=cache[0],
                                       category=cache[1].split('/')[1:-1][0],
                                       files=cache[2], version=cache[3],
                                       download=cache[4], download_x86_64=cache[5],
                                       md5sum=cache[6], md5sum_x86_64=cache[7],
                                       requires=cache[8], description=cache[9])

                self.session.add(sbo_data)

                cache = []  # reset cache after 11 lines

        self.session.commit()


# Delete the data before from the table.
session.query(SlackBuilds).delete()
session.commit()

args = sys.argv
args.pop(0)
path = ''

try:
    path = args[0]
except IndexError:
    pass

data = Data(path, version='15.0')
data.push()
