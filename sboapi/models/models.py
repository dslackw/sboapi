from dataclasses import dataclass
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Column, Integer, Text


DATABASE_URI = 'sqlite:///data.db'
engine = create_engine(DATABASE_URI,
                       connect_args={'check_same_thread': False})
session = sessionmaker(engine)()
Base = declarative_base()


@dataclass
class SlackBuilds(Base):
    __tablename__ = 'slackbuilds'

    id: int = Column(Integer, primary_key=True)
    name: str = Column(Text)
    category: str = Column(Text)
    files: str = Column(Text)
    version: str = Column(Text)
    download: str = Column(Text)
    download_x86_64: str = Column(Text)
    md5sum: str = Column(Text)
    md5sum_x86_64: str = Column(Text)
    requires: str = Column(Text)
    description: str = Column(Text)

    def serialized(self):

        return self.id, {
            'name': self.name,
            'category': self.category,
            'files': self.files.split(),
            'version': self.version,
            'download': self.download.split(),
            'download_x86_64': self.download_x86_64.split(),
            'md5sum': self.md5sum.split(),
            'md5sum_x86_64': self.md5sum_x86_64.split(),
            'requires': self.requires.split(),
            'description': self.description}


Base.metadata.create_all(engine)
