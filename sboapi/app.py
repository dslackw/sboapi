#!/usr/bin/python3
# -*- coding: utf-8 -*-

from flask import Flask, jsonify
from flask_restful import Api, Resource
from models.models import SlackBuilds, session
from sqlalchemy import and_


app = Flask(__name__)
api = Api(app)


class Packages(Resource):

    @staticmethod
    def get():
        query = session.query(SlackBuilds).all()

        return jsonify(dict([q.serialized() for q in query]))


class PackagesID(Resource):

    @staticmethod
    def get(package_id):
        query = session.query(SlackBuilds).filter(
            SlackBuilds.id == package_id).all()

        if query:
            return jsonify(dict([q.serialized() for q in query]))

        return {'Package ID': 'not found'}, 204


class PackagesName(Resource):

    @staticmethod
    def get(name):
        query = session.query(SlackBuilds).filter(
            SlackBuilds.name == name).all()

        if query:
            return jsonify(dict([q.serialized() for q in query]))

        return {'Package name': 'not found'}, 204


class PackagesCategory(Resource):

    @staticmethod
    def get(category, name):
        query = session.query(SlackBuilds).filter(
            and_(SlackBuilds.name == name,
                 SlackBuilds.category == category)).all()

        if query:
            return jsonify(dict([q.serialized() for q in query]))

        return {'Package name': 'not found'}, 204


api.add_resource(Packages, '/api/packages')
api.add_resource(PackagesID, '/api/packages/<int:package_id>')
api.add_resource(PackagesName, '/api/packages/<string:name>')
api.add_resource(PackagesCategory,
                 '/api/packages/<string:category>/<string:name>')


if __name__ == '__main__':
    app.run(port=5000, debug=True)
