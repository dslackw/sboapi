.. contents:: Table of Contents:


About
-----

RESTful API for the SlackBuilds.org repository.


Requirements
------------

.. code-block:: bash

    Flask==2.1.1
    Flask-RESTful==0.3.9
    SQLAlchemy==1.4.36

Getting the code
----------------

The code is hosted at https://gitlab.com/dslackw/sboapi

.. code-block:: bash

    $ git clone https://gitlab.com/dslackw/sboapi.git
    $ cd sboapi


Usage
-----

.. code-block:: bash

    $ python3 app.py


Endpoints
---------

.. code-block:: bash

    http://127.0.0.1:5000/api/packages  Get a list of packages
    http://127.0.0.1:5000/api/packages/<package_id>  Get a single package by package id
    http://127.0.0.1:5000/api/packages/<package_name>  Get a single package by package name
    http://127.0.0.1:5000/api/packages/<category>/<package_name>  Get a single package by category and name

    {
        "7608": {
            "category": "system",
            "description": "slpkg (Slackware Packaging Tool)",
            "download": [
                "https://gitlab.com/dslackw/slpkg/-/archive/3.9.8/slpkg-3.9.8.tar.gz"
            ],
            "download_x86_64": [],
            "files": [
                "README",
                "doinst.sh",
                "slack-desc",
                "slpkg.SlackBuild",
                "slpkg.info"
            ],
            "md5sum": [
                "41b3f4f0f8fb8270e3b03abd2c73be2c"
            ],
            "md5sum_x86_64": [],
            "name": "slpkg",
            "requires": [],
            "version": "3.9.8"
        }
    }


Deploy
------

See the project live on `pythonanywhere <https://www.pythonanywhere.com/>`_

Open your browser and go:

.. code-block:: bash

    http://dslackw.pythonanywhere.com/api/packages


Database
--------

Checks the dates between local and server file and creates the database.

.. code-block:: bash

    $ python3 data.py


Author
------

- Dimitris Zlatanidis

Contributor
-----------

- Dominik Drobek
